import React from 'react';

const VideoListItem = ({video, onVideoSelect}) => {
  return (
    <li onClick={() => onVideoSelect(video)} className="list-group-item">
      <div className="video-list media">
        <div className="media-left">
          <img src={video.snippet.thumbnails.default.url} className="media-object" alt="" />
        </div>
        <div className="media-body">
          <h4 className="media-heading">{video.snippet.title}</h4>
          {video.snippet.description}
        </div>
      </div>
    </li>
  );
};

export default VideoListItem;